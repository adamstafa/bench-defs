# Test-Comp Reproducibility - Overview
This repository describes the configuration of the competition machines (below)
and the benchmark definition for each test-case generator (folder [benchmark-defs/](benchmark-defs/)),
in order to make results of the competition reproducible.



# Components for Reproducing Competition Results

The competition uses several components to execute the benchmarks.
The components are described in the following table.

| Component               | Repository                                                      | Participants             |
| ---                     | ---                                                             | ---                      |
| Verification Tasks      | https://github.com/sosy-lab/sv-benchmarks                       | add, fix, review tasks   |
| Benchmark Definitions   | https://gitlab.com/sosy-lab/test-comp/bench-defs                | define their parameters  |
| Tool-Info Modules       | https://github.com/sosy-lab/benchexec/tree/main/benchexec/tools | define inferface         |
| Test-Generator Archives | https://gitlab.com/sosy-lab/test-comp/archives-2023             | submit to participate    |
| Benchmarking Framework  | https://github.com/sosy-lab/benchexec                           | (use to test their tool) |
| Competition Scripts     | https://gitlab.com/sosy-lab/benchmarking/competition-scripts    | (use to reproduce)       |
| Test-Suite Format       | https://gitlab.com/sosy-lab/software/test-format                | (know)                   |
| Task-Definition Format  | https://gitlab.com/sosy-lab/benchmarking/task-definition-format | (know)                   |
| Remote Execution        | https://gitlab.com/sosy-lab/software/coveriteam                 | (use to test their tool) |

Archives published at Zenodo:

| Year | Verification Tasks                      | Competition Results                     | Test Witnesses (Test Suites)            | BenchExec                               |
| ---  | ---                                     | ---                                     | ---                                     | ---                                     |
| 2023 |                                         |                                         |                                         |                                         |
| 2022 | https://doi.org/10.5281/zenodo.5831003  | https://doi.org/10.5281/zenodo.5831012  | https://doi.org/10.5281/zenodo.5831010  | https://doi.org/10.5281/zenodo.5720267  |
| 2021 | https://doi.org/10.5281/zenodo.4459132  | https://doi.org/10.5281/zenodo.4459470  | https://doi.org/10.5281/zenodo.4459466  | https://doi.org/10.5281/zenodo.4317433  |
| 2020 | https://doi.org/10.5281/zenodo.3678250  | https://doi.org/10.5281/zenodo.3678264  | https://doi.org/10.5281/zenodo.3678275  | https://doi.org/10.5281/zenodo.3574420  |
| 2019 | https://doi.org/10.5281/zenodo.3856478  | https://doi.org/10.5281/zenodo.3856661  | https://doi.org/10.5281/zenodo.3856669  | https://doi.org/10.5281/zenodo.2561835  |



# Instructions for Execution and Reproduction

Concrete instructions on how to execute the experiments and to reproduce the results of the competition are available here:
https://gitlab.com/sosy-lab/benchmarking/competition-scripts/#instructions-for-execution-and-reproduction



# Computing Environment on Competition Machines

## Installed Ubuntu packages

A description of all installed Ubuntu packages, with their versions is given here:
https://gitlab.com/sosy-lab/benchmarking/competition-scripts/#installed-ubuntu-packages

## Docker Image

Test-Comp provides a Docker image that tries to provide an environment
that has mostly the same packages installed as the competition machines.
The Docker image is described here:
https://gitlab.com/sosy-lab/benchmarking/competition-scripts/#container-image

## Parameters of RunExec

The parameters that are passed to the [BenchExec](https://github.com/sosy-lab/benchexec) [1]
executor [runexec](https://github.com/sosy-lab/benchexec/blob/main/doc/runexec.md) on the competition machines
are described here:
https://gitlab.com/sosy-lab/benchmarking/competition-scripts/#parameters-of-runexec


# References

[1]: Dirk Beyer, Stefan Löwe, and Philipp Wendler.
     Reliable Benchmarking: Requirements and Solutions.
     International Journal on Software Tools for Technology Transfer (STTT), 21(1):1-29, 2019.
     https://doi.org/10.1007/s10009-017-0469-y


